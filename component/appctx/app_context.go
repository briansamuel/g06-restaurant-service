package appctx

import (
	"g06-restaurant-service/kafka"
	"github.com/briansamuel/traceprovider"

	"gorm.io/gorm"
)

type AppContext interface {
	GetMainDBConnection() *gorm.DB
	SecretKey() string
	GetKafka() kafka.PubSub
	TraceProvider() traceprovider.Provider
}

type appCtx struct {
	db            *gorm.DB
	secretKey     string
	kafkaPs       kafka.PubSub
	traceProvider traceprovider.Provider
}

func NewAppContext(db *gorm.DB, secretKey string, kafkaPs kafka.PubSub, traceProvider traceprovider.Provider) *appCtx {
	return &appCtx{db: db, secretKey: secretKey, kafkaPs: kafkaPs, traceProvider: traceProvider}
}

func (ctx *appCtx) GetMainDBConnection() *gorm.DB { return ctx.db }

func (ctx *appCtx) SecretKey() string { return ctx.secretKey }

func (ctx *appCtx) GetKafka() kafka.PubSub { return ctx.kafkaPs }

func (ctx *appCtx) TraceProvider() traceprovider.Provider { return ctx.traceProvider }
