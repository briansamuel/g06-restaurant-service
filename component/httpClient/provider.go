package httpClient

import (
	"context"
	"net/http"
)

type HttpProvider interface {
	GetJson(req *http.Request, target interface{}) error
	SendGet(ctx context.Context, url, token string, target interface{}) error
	SendPost(ctx context.Context, url, token string, body, target interface{}) error
}
