package httpClient

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"g06-restaurant-service/common"
	trace "github.com/briansamuel/traceprovider/otel"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"time"
)

type httpClient struct {
	client *http.Client
}

func NewHttpClient() *httpClient {
	var myClient = &http.Client{Timeout: 30 * time.Second}
	return &httpClient{client: myClient}
}

func (c *httpClient) getJson(req *http.Request, target interface{}) error {

	r, err := c.client.Do(req)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println("Error while reading the response bytes:", err)
	}
	log.Println(string([]byte(body)))

	err = json.Unmarshal(body, target)
	if err != nil {
		return err
	}
	return nil

}

func (c *httpClient) GetJson(req *http.Request, target interface{}) error {

	err := c.getJson(req, target)
	if err != nil {
		return common.ErrEntityCannotParseJson("Interface", err)
	}
	return nil
}

func (c *httpClient) sendRequest(ctx context.Context, method, url, token string, body, target interface{}) error {
	tr := trace.Tracer()
	_, span := tr.Start(ctx, fmt.Sprintf("%s sendRequest", url))
	defer span.End()

	if method == "" {
		method = "GET"
	}

	var bearer = "Bearer " + token
	out, err := json.Marshal(body)
	if err != nil {
		log.Fatal(err)
	}
	req, err := http.NewRequest(method, url, bytes.NewBuffer(out))
	if err != nil {
		log.Fatal(err)
	}

	// add authorization header to the req
	req.Header.Add("Authorization", bearer)
	req.Header.Add("TraceID", span.SpanContext().TraceID().String())
	req.Header.Add("SpanID", span.SpanContext().SpanID().String())
	log.Info(span.SpanContext().SpanID().String())
	if err := c.getJson(req, target); err != nil {
		log.Fatal(err)
		return err
	}
	return nil
}

func (c *httpClient) SendGet(ctx context.Context, url, token string, target interface{}) error {

	err := c.sendRequest(ctx, "GET", url, token, nil, target)
	if err != nil {
		return common.ErrInvalidRequest(err)
	}
	return nil
}

func (c *httpClient) SendPost(ctx context.Context, url, token string, body, target interface{}) error {

	err := c.sendRequest(ctx, "POST", url, token, body, target)
	if err != nil {
		return common.ErrInvalidRequest(err)
	}
	return nil
}
