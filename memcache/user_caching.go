package memcache

import (
	"context"
	"encoding/json"
	"fmt"
	usermodel "g06-restaurant-service/modules/user/model"
	trace "github.com/briansamuel/traceprovider/otel"
	log "github.com/sirupsen/logrus"
	"sync"
)

type RealStore interface {
	FindUser(ctx context.Context, conditions map[string]interface{}) (*usermodel.User, error)
}

type UserCaching struct {
	store     Cache
	realStore RealStore
	once      *sync.Once
}

func NewUserCaching(store Cache, realStore RealStore) *UserCaching {
	return &UserCaching{
		store:     store,
		realStore: realStore,
		once:      new(sync.Once),
	}
}

func (uc *UserCaching) FindUser(ctx context.Context, conditions map[string]interface{}) (*usermodel.User, error) {

	tr := trace.Tracer()
	_, span := tr.Start(ctx, "Authentication")
	defer span.End()

	userId := conditions["id"].(int)

	key := fmt.Sprintf("user-%d", userId)

	userInCache := uc.store.Read(key)
	userCache := &usermodel.User{}

	if userInCache != nil {
		err := json.Unmarshal([]byte(userInCache.(string)), userCache)
		if err != nil {
			panic(err)
		}
		return userCache, nil
	} else {
		user, err := uc.realStore.FindUser(ctx, conditions)

		if err != nil {
			panic(err)
		}

		if user != nil {
			uc.store.WriteTTL(key, user, 3600)
			log.Info("Read UserCache", uc.store.Read(key))
			if err := json.Unmarshal([]byte(uc.store.Read(key).(string)), userCache); err != nil {
				return nil, err
			}

			return userCache, nil

		}

	}

	return userCache, nil
}
