package restaurantmodel

type RestaurantUpdate struct {
	Name    *string `json:"name" gorm:"column:name"`
	Address *string `json:"address" gorm:"column:addr"`
	Status  *int    `json:"status" gorm:"column:status"`
	Lat     float64 `json:"lat" gorm:"column:lat"`
	Lng     float64 `json:"lng" gorm:"column:lng"`
}

func (RestaurantUpdate) TableName() string { return Restaurant{}.TableName() }

func (data *RestaurantUpdate) Validate() error {
	if v := data.Name; v == nil || *v == "" {
		return ErrNameCannotBlank
	}

	return nil
}
