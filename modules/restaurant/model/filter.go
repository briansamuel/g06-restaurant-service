package restaurantmodel

import "g06-restaurant-service/common"

type Filter struct {
	common.SQLModel
	Name     string  `json:"name" gorm:"column:name"`
	Address  string  `json:"address" gorm:"column:addr"`
	OwnerId  int     `json:"owner_id" gorm:"column:owner_id"`
	Lat      float64 `json:"lat" gorm:"column:lat"`
	Lng      float64 `json:"lng" gorm:"column:lng"`
	Keyword  string  `json:"keyword" gorm:"keyword"`
	Distance int     `json:"distance" gorm:"distance"`
}

func (Filter) TableName() string { return Restaurant{}.TableName() }
