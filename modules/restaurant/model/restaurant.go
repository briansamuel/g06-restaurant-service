package restaurantmodel

import (
	"g06-restaurant-service/common"
)

const EntityName = "Restaurant"

type Restaurant struct {
	common.SQLModel
	Name      string             `json:"name" gorm:"column:name"`
	Address   string             `json:"address" gorm:"column:addr"`
	OwnerId   int                `json:"owner_id" gorm:"column:owner_id"`
	Logo      common.Image       `json:"logo" gorm:"logo"`
	Cover     common.Images      `json:"cover" gorm:"cover"`
	User      *common.SimpleUser `json:"user" gorm:"foreignKey:OwnerId;preload:false;"`
	LikeCount int                `json:"like_count" gorm:"like_count"`
	Lat       float64            `json:"lat" gorm:"column:lat"`
	Lng       float64            `json:"lng" gorm:"column:lng"`
	Distance  float64            `json:"distance" gorm:"_"`
}

func (Restaurant) TableName() string { return "restaurants" }

func (data *Restaurant) Mask(isOwnerOrAdmin bool) {
	data.GenUID(common.DbTypeRestaurant)

	if u := data.User; u != nil {
		u.Mask(isOwnerOrAdmin)
	}
}
