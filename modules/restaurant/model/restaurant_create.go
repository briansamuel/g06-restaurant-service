package restaurantmodel

import (
	"g06-restaurant-service/common"
)

type RestaurantCreate struct {
	common.SQLModel
	Name    string         `json:"name" gorm:"unique;column:name;"  binding:"required" `
	Address string         `json:"address" gorm:"column:addr" binding:"required"`
	OwnerId int            `json:"owner_id" gorm:"column:owner_id"`
	Logo    *common.Image  `json:"logo" gorm:"logo"`
	Cover   *common.Images `json:"cover" gorm:"cover"`
	Lat     float64        `json:"lat" gorm:"column:lat"`
	Lng     float64        `json:"lng" gorm:"column:lng"`
}

func (RestaurantCreate) TableName() string { return Restaurant{}.TableName() }

func (data *RestaurantCreate) Validate() error {
	if v := data.Name; &v == nil || v == "" {
		return ErrNameCannotBlank
	}

	return nil
}

func (data *RestaurantCreate) Mask(isOwnerOrAdmin bool) {
	data.GenUID(common.DbTypeRestaurant)
}
