package bizrestaurant

import (
	"context"
	"g06-restaurant-service/common"
	restaurantmodel "g06-restaurant-service/modules/restaurant/model"
	trace "github.com/briansamuel/traceprovider/otel"
)

type ListRestaurantStore interface {
	ListDataWithCondition(
		ctx context.Context,
		filter *restaurantmodel.Filter,
		paging *common.Paging) ([]restaurantmodel.Restaurant, error)
	NearByRestaurantWithCondition(ctx context.Context,
		filter *restaurantmodel.Filter,
		paging *common.Paging) ([]restaurantmodel.Restaurant, error)
}

type RestaurantLikesStorage interface {
	GetRestaurantLike(ctx context.Context, ids []int) (map[int]int, error)
}

type listRestaurantBiz struct {
	store        ListRestaurantStore
	storeResLike RestaurantLikesStorage
}

func NewListRestaurantBiz(store ListRestaurantStore, storeResLike RestaurantLikesStorage) *listRestaurantBiz {
	return &listRestaurantBiz{store: store, storeResLike: storeResLike}
}

func (biz *listRestaurantBiz) ListRestaurant(
	ctx context.Context,
	filter *restaurantmodel.Filter,
	paging *common.Paging) ([]restaurantmodel.Restaurant, error) {

	tr := trace.Tracer()
	ctx, span := tr.Start(ctx, "Business List Restaurant")
	defer span.End()

	result, err := biz.store.ListDataWithCondition(ctx, filter, paging)

	if err != nil {
		return nil, common.ErrCannotListEntity(restaurantmodel.EntityName, err)
	}

	// Linking with restaurant_likes table
	//resIDs := make([]int, len(result))
	//for i := range resIDs {
	//	resIDs[i] = result[i].ID
	//}
	//
	//mapResLiked, err := biz.storeResLike.GetRestaurantLike(ctx, resIDs)
	//if err == nil {
	//	for i := range result {
	//		result[i].LikeCount = mapResLiked[result[i].ID]
	//	}
	//}

	return result, nil
}

func (biz *listRestaurantBiz) NearByRestaurants(
	ctx context.Context,
	filter *restaurantmodel.Filter,
	paging *common.Paging) ([]restaurantmodel.Restaurant, error) {

	tr := trace.Tracer()
	ctx, span := tr.Start(ctx, "Business List Restaurant")
	defer span.End()

	result, err := biz.store.NearByRestaurantWithCondition(ctx, filter, paging)

	if err != nil {
		return nil, common.ErrCannotListEntity(restaurantmodel.EntityName, err)
	}

	// Linking with restaurant_likes table
	//resIDs := make([]int, len(result))
	//for i := range resIDs {
	//	resIDs[i] = result[i].ID
	//}
	//
	//mapResLiked, err := biz.storeResLike.GetRestaurantLike(ctx, resIDs)
	//if err == nil {
	//	for i := range result {
	//		result[i].LikeCount = mapResLiked[result[i].ID]
	//	}
	//}

	return result, nil
}
