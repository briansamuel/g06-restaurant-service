package bizrestaurant

import (
	"context"
	"g06-restaurant-service/common"
	restaurantmodel "g06-restaurant-service/modules/restaurant/model"
	trace "github.com/briansamuel/traceprovider/otel"
)

type CreateStore interface {
	Create(ctx context.Context, data *restaurantmodel.RestaurantCreate) error
}

type createRestaurantBiz struct {
	store CreateStore
}

func NewCreateRestaurantBiz(store CreateStore) *createRestaurantBiz {
	return &createRestaurantBiz{store: store}
}

func (biz *createRestaurantBiz) CreateRestaurant(ctx context.Context, data *restaurantmodel.RestaurantCreate) error {

	tr := trace.Tracer()
	ctx, span := tr.Start(ctx, "Business Create Restaurant")
	defer span.End()
	err := biz.store.Create(ctx, data)
	if err != nil {
		return common.ErrCannotCreateEntity(restaurantmodel.EntityName, err)
	}

	return nil

}
