package bizrestaurant

import (
	"context"
	"g06-restaurant-service/common"
	restaurantmodel "g06-restaurant-service/modules/restaurant/model"
	trace "github.com/briansamuel/traceprovider/otel"
)

type DeleteStore interface {
	GetDataWithCondition(
		ctx context.Context,
		cond map[string]interface{}) (*restaurantmodel.Restaurant, error)
	Delete(ctx context.Context, id int) error
	Update(ctx context.Context, id int, data *restaurantmodel.RestaurantUpdate) error
}

type deleteRestaurantBiz struct {
	store DeleteStore
}

func NewDeleteRestaurantBiz(store DeleteStore) *deleteRestaurantBiz {
	return &deleteRestaurantBiz{store: store}
}

func (biz *deleteRestaurantBiz) DeleteRestaurant(ctx context.Context, id int, isSoft bool) error {
	oldData, err := biz.store.GetDataWithCondition(ctx, map[string]interface{}{"id": id})
	tr := trace.Tracer()
	ctx, span := tr.Start(ctx, "Business Delete Restaurant")
	defer span.End()
	if err != nil {

		return err
	}
	if oldData.Status == 0 {
		return common.ErrEntityDeleted(restaurantmodel.EntityName, err)
	}
	if isSoft {
		zero := 0
		if err := biz.store.Update(ctx, id, &restaurantmodel.RestaurantUpdate{Status: &zero}); err != nil {

			return common.ErrCannotDeleteEntity(restaurantmodel.EntityName, err)
		}
		return nil
	}

	if err := biz.store.Delete(ctx, id); err != nil {

		return err
	}
	return nil
}
