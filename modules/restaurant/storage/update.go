package restaurantstore

import (
	"context"
	"g06-restaurant-service/common"
	restaurantmodel "g06-restaurant-service/modules/restaurant/model"
	"go.opentelemetry.io/otel"
	"gorm.io/gorm"
	"os"
)

func (s *sqlStore) Update(ctx context.Context, id int, data *restaurantmodel.RestaurantUpdate) error {

	tr := otel.Tracer(os.Getenv("SERVICE_NAME"))
	_, span := tr.Start(ctx, "Authentication")
	defer span.End()
	db := s.db.WithContext(ctx)

	if err := db.Where("id = ?", id).Updates(&data).Error; err != nil {
		return common.ErrDB(err)
	}

	return nil
}

func (s *sqlStore) IncreaseLikeCount(ctx context.Context, id int) error {
	db := s.db.WithContext(ctx)

	if err := db.Table(restaurantmodel.Restaurant{}.TableName()).Where("id = ?", id).
		Update("like_count", gorm.Expr("like_count + ?", 1)).Error; err != nil {
		return common.ErrDB(err)
	}

	return nil
}

func (s *sqlStore) DecreaseLikeCount(ctx context.Context, id int) error {
	db := s.db.WithContext(ctx)

	if err := db.Table(restaurantmodel.Restaurant{}.TableName()).Where("id = ?", id).
		Update("like_count", gorm.Expr("like_count - ?", 1)).Error; err != nil {
		return common.ErrDB(err)
	}

	return nil
}

// Update
