package restaurantstore

import (
	"context"
	"g06-restaurant-service/common"
	restaurantmodel "g06-restaurant-service/modules/restaurant/model"
	trace "github.com/briansamuel/traceprovider/otel"
	"go.opentelemetry.io/otel/attribute"
)

func (s *sqlStore) Create(ctx context.Context, data *restaurantmodel.RestaurantCreate) error {

	tr := trace.Tracer()
	ctx, span := tr.Start(ctx, "Storage Create Restaurant")
	defer span.End()
	db := s.db.WithContext(ctx)
	if err := db.Create(data).Error; err != nil {
		return common.ErrDB(err)
	}
	span.SetAttributes(attribute.Key("restaurant_id").Int(data.ID))

	return nil
}
