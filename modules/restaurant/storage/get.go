package restaurantstore

import (
	"context"
	"g06-restaurant-service/common"
	"gorm.io/gorm"

	restaurantmodel "g06-restaurant-service/modules/restaurant/model"
)

func (s *sqlStore) GetDataWithCondition(ctx context.Context,
	cond map[string]interface{}) (*restaurantmodel.Restaurant, error) {
	db := s.db.WithContext(ctx)
	var data restaurantmodel.Restaurant
	if err := db.
		Where(cond).
		First(&data).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, common.RecordNotFound
		}
		return nil, common.ErrDB(err)
	}

	return &data, nil
}
