package restaurantstore

import (
	"context"
	"g06-restaurant-service/common"
	restaurantmodel "g06-restaurant-service/modules/restaurant/model"
)

func (s *sqlStore) ListDataWithCondition(ctx context.Context,
	filter *restaurantmodel.Filter,
	paging *common.Paging) ([]restaurantmodel.Restaurant, error) {
	//tr := otel.Tracer(os.Getenv("SERVICE_NAME"))

	db := s.db.WithContext(ctx)
	var result []restaurantmodel.Restaurant

	db = db.Where("status in (?)", 1)

	if filter.OwnerId > 0 {
		db = db.Where("owner_id = ?", filter.OwnerId)
	}

	if err := db.Table(restaurantmodel.Restaurant{}.TableName()).Count(&paging.Total).Error; err != nil {
		return nil, common.ErrDB(err)
	}

	if err := db.
		Limit(paging.Limit).
		Offset((paging.Page - 1) * paging.Limit).
		Order("id desc").
		Find(&result).Error; err != nil {

		return nil, common.ErrDB(err)
	}

	return result, nil
}

func (s *sqlStore) NearByRestaurantWithCondition(ctx context.Context,
	filter *restaurantmodel.Filter,
	paging *common.Paging) ([]restaurantmodel.Restaurant, error) {
	//tr := otel.Tracer(os.Getenv("SERVICE_NAME"))

	db := s.db.WithContext(ctx)
	var result []restaurantmodel.Restaurant
	var distance = 2

	if filter.Distance > 0 {
		distance = filter.Distance
	}
	db = db.Where("status in (?)", 1)

	var distanceCalculation = `
       (
           (
                   6371.04 * ACOS(((COS(((PI() / 2) - RADIANS((90 - lat)))) *
                                    COS(PI() / 2 - RADIANS(90 - ?)) *
                                    COS((RADIANS(lng) - RADIANS(?))))
                   + (SIN(((PI() / 2) - RADIANS((90 - lat)))) *
                      SIN(((PI() / 2) - RADIANS(90 - ?))))))
               )
           )`

	if filter.OwnerId > 0 {
		db = db.Where("owner_id = ?", filter.OwnerId)
	}

	if filter.Keyword != "" {
		db = db.Where("name LIKE ?", "%"+filter.Keyword+"%")
	}

	if filter.Lat != 0 && filter.Lng != 0 {

	}

	//if err := db.Table(restaurantmodel.Restaurant{}.TableName()).
	//	Select(" count(*), "+distanceCalculation+" as distance\n", filter.Lat, filter.Lng, filter.Lat).
	//	Having("distance < 2").
	//	Scan(&paging.Total).Error; err != nil {
	//	return nil, common.ErrDB(err)
	//}

	if err := db.
		Table(restaurantmodel.Restaurant{}.TableName()).
		Select("*, "+
			distanceCalculation+" as distance\n", filter.Lat, filter.Lng, filter.Lat).
		Having("distance < ?", distance).
		Limit(paging.Limit).
		Offset((paging.Page - 1) * paging.Limit).
		Order("distance asc").
		Scan(&result).Error; err != nil {

		return nil, common.ErrDB(err)
	}
	paging.Total = int64(len(result))
	return result, nil
}
