package ginrestaurant

import (
	"g06-restaurant-service/common"
	"g06-restaurant-service/component/appctx"
	bizrestaurant "g06-restaurant-service/modules/restaurant/business"
	restaurantmodel "g06-restaurant-service/modules/restaurant/model"
	restaurantstore "g06-restaurant-service/modules/restaurant/storage"
	restaurantlikestorage "g06-restaurant-service/modules/restaurantlike/storage"
	"github.com/gin-gonic/gin"
	"net/http"
)

func ListRestaurant(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {

		var filter restaurantmodel.Filter
		var paging common.Paging
		paging.Process()
		if err := c.ShouldBind(&filter); err != nil {
			panic(common.ErrInternal(err))
		}
		parentContext := appContext.TraceProvider().ParentContext(c.Request.Context(), c.GetString("TraceID"), c.GetString("SpanID"))

		store := restaurantstore.NewSQLStore(appContext.GetMainDBConnection())
		storeResLike := restaurantlikestorage.NewSQLStore(appContext.GetMainDBConnection())
		biz := bizrestaurant.NewListRestaurantBiz(store, storeResLike)

		result, err := biz.ListRestaurant(parentContext, &filter, &paging)
		if err != nil {
			panic(err)
		}

		for i := range result {
			result[i].Mask(false)
		}
		c.JSON(http.StatusOK, common.NewSuccessResponse(result, paging, filter))
	}
}

func NearByRestaurant(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {

		var filter restaurantmodel.Filter
		var paging common.Paging
		paging.Process()
		if err := c.ShouldBind(&filter); err != nil {
			panic(common.ErrInternal(err))
		}
		parentContext := appContext.TraceProvider().ParentContext(c.Request.Context(), c.GetString("TraceID"), c.GetString("SpanID"))

		store := restaurantstore.NewSQLStore(appContext.GetMainDBConnection())
		storeResLike := restaurantlikestorage.NewSQLStore(appContext.GetMainDBConnection())
		biz := bizrestaurant.NewListRestaurantBiz(store, storeResLike)

		result, err := biz.NearByRestaurants(parentContext, &filter, &paging)
		if err != nil {
			panic(err)
		}

		for i := range result {
			result[i].Mask(false)
		}
		c.JSON(http.StatusOK, common.NewSuccessResponse(result, paging, filter))
	}
}
