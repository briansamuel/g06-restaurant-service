package ginrestaurant

import (
	"g06-restaurant-service/common"
	"g06-restaurant-service/component/appctx"
	bizrestaurant "g06-restaurant-service/modules/restaurant/business"
	restaurantmodel "g06-restaurant-service/modules/restaurant/model"
	restaurantstore "g06-restaurant-service/modules/restaurant/storage"
	"github.com/gin-gonic/gin"
	"net/http"
)

func UpdateRestaurant(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {
		id, err := common.FromBase58(c.Param("id"))

		if err != nil {
			panic(common.ErrInternal(err))
		}
		var data restaurantmodel.RestaurantUpdate
		if err := c.ShouldBind(&data); err != nil {
			panic(common.ErrInternal(err))
		}

		store := restaurantstore.NewSQLStore(appContext.GetMainDBConnection())
		biz := bizrestaurant.NewUpdateRestaurantBiz(store)

		if err := biz.UpdateRestaurant(c.Request.Context(), int(id.GetLocalID()), &data); err != nil {
			panic(err)
		}
		c.JSON(http.StatusOK, common.SimpleSuccessResponse(data))
	}
}
