package ginrestaurant

import (
	"g06-restaurant-service/common"
	"g06-restaurant-service/component/appctx"
	bizrestaurant "g06-restaurant-service/modules/restaurant/business"
	restaurantstore "g06-restaurant-service/modules/restaurant/storage"
	"github.com/gin-gonic/gin"
	"net/http"
)

func GetRestaurant(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {

		id, err := common.FromBase58(c.Param("id"))

		if err != nil {
			panic(common.ErrInvalidRequest(err))
		}

		store := restaurantstore.NewSQLStore(appContext.GetMainDBConnection())
		biz := bizrestaurant.NewGetRestaurantBiz(store)

		data, err := biz.GetRestaurant(c.Request.Context(), int(id.GetLocalID()))
		if err != nil {
			panic(err)
		}
		data.Mask(false)
		c.JSON(http.StatusOK, common.SimpleSuccessResponse(data))
	}
}
