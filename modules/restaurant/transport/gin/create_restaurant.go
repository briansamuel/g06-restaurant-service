package ginrestaurant

import (
	"g06-restaurant-service/common"
	"g06-restaurant-service/component/appctx"
	bizrestaurant "g06-restaurant-service/modules/restaurant/business"
	restaurantmodel "g06-restaurant-service/modules/restaurant/model"
	restaurantstore "g06-restaurant-service/modules/restaurant/storage"
	"github.com/go-playground/validator/v10"
	"net/http"

	"github.com/gin-gonic/gin"
)

func CreateRestaurant(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {
		var newRestaurant restaurantmodel.RestaurantCreate

		if err := c.ShouldBind(&newRestaurant); err != nil {
			panic(common.ErrInternal(err))
		}
		validate := validator.New()

		if err := validate.Struct(newRestaurant); err != nil {
			if _, ok := err.(*validator.InvalidValidationError); ok {
				panic(common.ErrValidationError(restaurantmodel.EntityName, err))

			}

			for _, err := range err.(validator.ValidationErrors) {
				panic(common.ErrValidationError(err.Field(), err))
			}
		}

		requester := c.MustGet(common.CurrentUser).(common.Requester)

		newRestaurant.OwnerId = requester.GetUserId()
		store := restaurantstore.NewSQLStore(appContext.GetMainDBConnection())
		biz := bizrestaurant.NewCreateRestaurantBiz(store)

		if err := biz.CreateRestaurant(c.Request.Context(), &newRestaurant); err != nil {
			panic(err)
		}

		c.JSON(http.StatusOK, common.SimpleSuccessResponse(newRestaurant.ID))
	}
}
