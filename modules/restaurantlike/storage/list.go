package restaurantlikestore

import (
	"context"
	"g06-restaurant-service/common"
	restaurantlikemodel "g06-restaurant-service/modules/restaurantlike/model"
	"gorm.io/gorm"
)

func (s *sqlStore) GetUsersLikeRestaurant(ctx context.Context,
	conditions map[string]interface{},
	filter *restaurantlikemodel.Filter,
	paging *common.Paging,
	moreKeys ...string) ([]restaurantlikemodel.User, error) {

	db := s.db.Session(&gorm.Session{NewDB: true})
	var result []restaurantlikemodel.Like

	db = db.Where(conditions)

	if filter.RestaurantID > 0 {
		db = db.Where("restaurant_id = ?", filter.RestaurantID)
	}

	if err := db.
		Limit(paging.Limit).
		Offset((paging.Page - 1) * paging.Limit).
		Order("created_at desc").
		Find(&result).Error; err != nil {

		return nil, common.ErrDB(err)
	}
	//users := make([]restaurantlikemodel.User, len(result))
	//
	//for i, item := range result {
	//	result[i].User.CreatedAt = item.CreatedAt
	//	users[i] = *result[i].User
	//}
	return nil, nil
}

func (s *sqlStore) GetRestaurantLike(ctx context.Context, ids []int) (map[int]int, error) {
	result := make(map[int]int)

	type sqlData struct {
		RestaurantId int `json:"restaurant_id"`
		LikeCount    int `json:"count"`
	}
	var listLike []sqlData

	if err := s.db.Table(restaurantlikemodel.Like{}.TableName()).
		Select("restaurant_id, count(restaurant_id) as count").
		Where("restaurant_id in (?)", ids).
		Group("restaurant_id").Find(&listLike).Error; err != nil {
		return nil, err
	}

	for _, item := range listLike {
		result[item.RestaurantId] = item.LikeCount
	}

	return result, nil
}
