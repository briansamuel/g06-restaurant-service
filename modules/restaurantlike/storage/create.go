package restaurantlikestore

import (
	"context"
	"g06-restaurant-service/common"
	restaurantlikemodel "g06-restaurant-service/modules/restaurantlike/model"
)

func (s *sqlStore) Create(ctx context.Context, data *restaurantlikemodel.Like) error {
	db := s.db

	if err := db.Table(restaurantlikemodel.Like{}.TableName()).Create(data).Error; err != nil {
		return common.ErrDB(err)
	}

	return nil
}
