package restaurantlikemodel

import (
	"g06-restaurant-service/common"
	"time"
)

const EntityName = "UserLikeRestaurant"

type Like struct {
	RestaurantID int        `json:"restaurant_id" gorm:"column:restaurant_id"`
	UserId       int        `json:"user_id" gorm:"column:user_id"`
	CreatedAt    *time.Time `json:"created_at" gorm:"column:created_at;"`
	User         *User      `json:"user" gorm:"preload:false;"`
}

func (Like) TableName() string { return "restaurant_likes" }
func (l *Like) GetRestaurantId() int {
	return l.RestaurantID
}

func (l *Like) GetUserId() int {
	return l.UserId
}

type User struct {
	ID        int           `json:"-" gorm:"id"`
	FakeId    common.UID    `json:"id" gorm:"-"`
	LastName  string        `json:"last_name" gorm:"column:last_name;"`
	FirstName string        `json:"first_name" gorm:"column:first_name;"`
	Role      string        `json:"role" gorm:"role"`
	Avatar    *common.Image `json:"avatar" gorm:"avatar"`
	CreatedAt *time.Time    `json:"created_at" gorm:"column:created_at;"`
}

func (User) TableName() string { return "users" }

func (u *User) Mask(isAdmin bool) {
	u.FakeId = common.NewUID(uint32(u.ID), common.DbTypeUser, 1)
}
