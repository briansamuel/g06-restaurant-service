package main

import (
	"context"
	"fmt"
	"g06-restaurant-service/common"
	"g06-restaurant-service/component/appctx"
	"g06-restaurant-service/kafka"
	"g06-restaurant-service/kafka/kafkapb"
	"g06-restaurant-service/memcache"
	"g06-restaurant-service/middleware"
	ginrestaurant "g06-restaurant-service/modules/restaurant/transport/gin"
	userstorage "g06-restaurant-service/modules/user/storage"
	trace "github.com/briansamuel/traceprovider/otel"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis/v8"
	"github.com/joho/godotenv"
	"github.com/mattn/go-colorable"
	log "github.com/sirupsen/logrus"
	"github.com/uptrace/opentelemetry-go-extra/otelgorm"
	"go.opentelemetry.io/contrib/instrumentation/github.com/gin-gonic/gin/otelgin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"os"
)

func main() {

	formatter := new(log.TextFormatter)
	formatter.ForceColors = true
	formatter.TimestampFormat = "2006-01-02 15:04:05"
	formatter.FullTimestamp = true
	log.SetFormatter(formatter)
	log.SetOutput(colorable.NewColorableStdout())
	// Config Log
	if os.Getenv("GIN_MODE") == "RELEASE" {
		log.SetLevel(log.InfoLevel)
	} else {
		log.SetLevel(log.TraceLevel)
	}

	log.Info("Setup Config Log")
	// End Config Log

	err := godotenv.Load()
	if err != nil {
		log.Info("Error loading .env file")
	}

	dbHost := os.Getenv("DB_HOST")
	dbDatabase := os.Getenv("DB_NAME")
	dbPort := os.Getenv("DB_PORT")
	dbUser := os.Getenv("DB_USER")
	dbPassword := os.Getenv("DB_PASSWORD")
	secretKey := os.Getenv("SYSTEM_SECRET")
	ginPort := os.Getenv("GIN_PORT")
	jaegerService := os.Getenv("JAEGER_SERVICE")
	serviceName := os.Getenv("SERVICE_NAME")
	kafkaService := os.Getenv("KAFKA_SERVICE")
	redisService := os.Getenv("REDIS_SERVICE")
	redisPass := os.Getenv("REDIS_PASSWORD")
	mysqlConnection := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=true&loc=Local", dbUser, dbPassword, dbHost, dbPort, dbDatabase)

	db, err := gorm.Open(mysql.Open(mysqlConnection), &gorm.Config{})
	log.Info("Connect Mysql")
	if err != nil {
		log.Fatalln("Error mysql:", err)
		panic(err)
	}

	if err := db.Use(otelgorm.NewPlugin()); err != nil {
		panic(err)
	}
	db = db.Debug()

	rdb := redis.NewClient(&redis.Options{
		Addr:     redisService,
		Password: redisPass, // no password set
		DB:       0,         // use default DB
	})

	if _, err := rdb.Ping(context.Background()).Result(); err != nil {
		log.Fatalln(err)
	}
	log.Info("Setup Gin Router")
	r := gin.Default()
	common.ListService = map[string]string{
		"AUTH_SERVICE":       os.Getenv("AUTH_SERVICE"),
		"RESTAURANT_SERVICE": os.Getenv("RESTAURANT_SERVICE"),
		"UPLOAD_SERVICE":     os.Getenv("UPLOAD_SERVICE"),
	}

	client := kafkapb.NewKafkaPubSub(kafkaService)

	trace := trace.NewTraceProvider(serviceName, jaegerService)

	appCtx := appctx.NewAppContext(db, secretKey, client, trace)
	kafka.NewSubscriber(appCtx).Start()

	r.Use(middleware.Recover(appCtx))
	r.Use(otelgin.Middleware(serviceName))

	authStore := userstorage.NewSQLStore(appCtx.GetMainDBConnection())
	userCaching := memcache.NewUserCaching(memcache.NewRedisCaching(rdb), authStore)
	middlewareAuth := middleware.RequireAuth(appCtx, userCaching)
	v1 := r.Group("/v1")
	{
		restaurants := v1.Group("/restaurants", middlewareAuth)
		{
			restaurants.POST("", ginrestaurant.CreateRestaurant(appCtx))

			restaurants.GET("", ginrestaurant.ListRestaurant(appCtx))

			restaurants.GET("/:id", ginrestaurant.GetRestaurant(appCtx))

			restaurants.PUT("/:id", ginrestaurant.UpdateRestaurant(appCtx))

			restaurants.DELETE("/:id", ginrestaurant.DeleteRestaurant(appCtx))

			restaurants.POST("/nearby", ginrestaurant.NearByRestaurant(appCtx))

		}
	}

	trace.SetProvider()

	err = r.Run(ginPort)
	if err != nil {
		log.Fatalln(err)
	} // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}
