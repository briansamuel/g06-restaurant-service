package kafka

import (
	"context"
	"g06-restaurant-service/common"
	restaurantstore "g06-restaurant-service/modules/restaurant/storage"
	restaurantlikemodel "g06-restaurant-service/modules/restaurantlike/model"
)

func (sb *subscriber) Setup() {

	sb.startSubTopic(
		common.TopicUserDislikeRestaurant,
		true,
		DecreaseLikeCountAfterUserDisLikeRestaurant(sb.appCtx),
	)

	// Follow topic in queue
	sb.startSubTopic(
		common.TopicUserLikeRestaurant,
		true,
		IncreaseLikeCountAfterUserLikeRestaurant(sb.appCtx),
	)

}

func IncreaseLikeCountAfterUserLikeRestaurant(appCtx AppContext) ConsumerJob {
	return ConsumerJob{
		Title: "Increase like count after user like restaurant",
		Hdl: func(ctx context.Context, msg *Message) error {

			store := restaurantstore.NewSQLStore(appCtx.GetMainDBConnection())
			likeData := &restaurantlikemodel.Like{}
			BodyParse(msg.Body, likeData)

			return store.IncreaseLikeCount(ctx, likeData.RestaurantID)

		},
	}
}

func DecreaseLikeCountAfterUserDisLikeRestaurant(appCtx AppContext) ConsumerJob {
	return ConsumerJob{
		Title: "Decrease like count after user dislike restaurant",
		Hdl: func(ctx context.Context, msg *Message) error {

			store := restaurantstore.NewSQLStore(appCtx.GetMainDBConnection())
			likeData := &restaurantlikemodel.Like{}
			BodyParse(msg.Body, likeData)

			return store.DecreaseLikeCount(ctx, likeData.RestaurantID)

		},
	}
}
